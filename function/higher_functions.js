function not(f){
    return function(){
        var result = f.apply(this, arguments);
        return !result;
    }
}

var even = function(x){
    return x%2 === 0;
}

var result = [2,8,6].every(even);
var odd = not(even);
var result2 = [1,3,5,7,9].every(odd); //true

/***************mapper*******************/

var data = [1,2,3,4,5]

function mapper(f){
    return function(a){return a.map(f)};
}

var increment = function(x){return x + 1;}

//var result3 = data.map(function(x){return x +1});
var incrementer = mapper(increment);

//console.log(result3);
console.log(incrementer(data));
