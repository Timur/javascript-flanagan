function add(x,y){return x + y;}
function substract(x,y){return x - y;}
function multiply(x,y){return x * y;}
function divide(x,y){return x / y;}

function operate(operator, operand1, operand2){
    return operator(operand1, operand2);
}

var i = operate(add, operate(add, 6, 9), operate(multiply, 58, 888));
//console.log(i);

var operators = {
    add: function(x,y){return x + y;},
    substract: function(x,y){return x - y;},
    multiply: function(x,y){return x * y;},
    divide: function(x,y){return x / y;},
    pow: Math.pow
}

function operate2(operator, operand1, operand2){
    if(typeof operators[operator] === "function"){
        return operators[operator](operand1, operand2);
    } else {
        throw "Неизвестный оператор";
    }
}


//function properties


function uniqueInteger(){
    return uniqueInteger.counter ++
}
uniqueInteger.counter = 0;

console.log(uniqueInteger());
console.log(uniqueInteger());
console.log(uniqueInteger());

function factorial(n){
   if (isFinite(n) && n>0 && n==Math.round(n)){
       if(!(n in factorial)){
           factorial[n] = n * factorial(n-1);
       }
       return factorial[n];
   }
   else return NaN;
}
factorial[1] = 1;
console.log(factorial(3));