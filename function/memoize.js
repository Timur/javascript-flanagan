function memoize(f){
    var cache = {};
    return function(){
        var key = arguments.length + Array.prototype.join.call(arguments, ",");
        if(key in cache){
            return cache[key];
        } else {
            cache[key] = f.apply(this, arguments);
            return cache[key];
        }
    }
}

//НОД, алгоритм Эвклида
function gcd(a, b){
    var t;
    if(a < b){
        t = b, b = a, a = t;
    }
    while(b != 0){
        t = b, b = a%b, a = t;
    }
    return a;
}

var gcdmemo = memoize(gcd);
//console.log(gcdmemo(85, 187))
//console.log(gcdmemo(85, 187))
//console.log(gcdmemo(85, 187))

//При мемоизации рекурсивных функций желательно что бы рекурсия
//выполнялась в мемоизированной версии, а не в оригинале

var factorial = memoize(function(n){
    return (n <= 1) ? 1 : n * factorial(n-1);
});

console.log(factorial(5));
console.log(factorial(4));
console.log(factorial(3));