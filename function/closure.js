function addPrivateProperty(o, name, predicate){
    var value;
    o["get"+name] = function(){ return value };
    o["set"+name] = function(v){
        if(predicate && !predicate(v)){
            throw Error("set" + name + ": недопустимое значение " + v);
        } else {
            value = v;
        }
    }
}

var o = {};

addPrivateProperty(o, "Name", function(x){
    return typeof x == "string";
})

//o.setName("Frank");
//console.log(o.getName());
//o.setName(0);

var uniqueInteger = (function(){
    var counter = 0;
    return function(){
        return counter ++;
    }
}());

//console.log(uniqueInteger());
//console.log(uniqueInteger());
//console.log(uniqueInteger());

function counter(){
    var n = 0;
    return {
        count: function(){return n++;},
        reset: function(){n = 0;}
    }
}

var c = counter();
var d = counter();

console.log(c.count() + "--" + d.count());
console.log(c.count() + "--" + d.count());
console.log(c.count() + "--" + d.count());
c.reset();
console.log(c.count() + "--" + d.count());
console.log(c.count() + "--" + d.count());
console.log(c.count() + "--" + d.count());
