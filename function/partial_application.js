//function array(a, n){
//    return Array.prototype.slice.call(a, n || 0);
//}
//
//function partialLeft(){
//
//}

function partial_one_arg(f, a){
    return function(b){
        return f(a, b);
    }
}

min0 = partial_one_arg(Math.min, 0);
min1 = partial_one_arg(Math.min, 1);

console.log([min0(-5), min0(5)]); // вернёт [-5, 0]
console.log([min1(-5), min1(5)]); // вернёт [-5, 1]