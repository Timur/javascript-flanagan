o = {
    test_func: function(){
        console.log(2)
    }
}
var f = function(){
    var sum = arguments[0] + arguments[1]
    console.log(sum);
}

o.m = f;

//o.m(55, 88);
//f.call(o, 8, 9);

//f.apply(o, [8, 9]);

//Monkey patching
Function.prototype.trace = function(o){
    var original = o[this];
    o[this] = function(){
        console.log(new Date(), "Entering:", m);
        var result = original.apply(this, arguments);
        console.log(new Date(), "Exiting:", m);
        return result;
    };
}

f.trace(o);