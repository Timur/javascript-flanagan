var o = {x: 1};
function f(y){
    return this.x + y;
}

var g = f.bind(o);


//currying
var sum = function(x, y){
    return x + y;
}

var suc = sum.bind(null, 1);
suc(2)  // 3

function f(y, z){
    return this.x + y + z;
}

var g = f.bind({x: 1}, 2);

console.log(g(6));