function Set(){
    this.values = {};
    this.n = 0;
    this.add.apply(this, arguments);
}

Set.prototype.add = function(){
    for(var i = 0; i < arguments.length; i++){
        var val = arguments[i];
        var str = Set._v2s(val);
        if(!this.values.hasOwnProperty(str)){
            this.values[str] = val;
            this.n++;
        }
    }
    return this;
};

Set._v2s = function(val){
    switch(val){
        case undefined: return 'u';
        case null: return 'n';
        case true: return 't';
        case false: return 'f';
        default :switch (typeof val){
            case 'number': return '#' + val;
            case 'string': return '"' + val;
            default : return '@' + objectId(val);
        }
    }

    function objectId(o){
        var prop = '|**objectid**|';
        if(!o.hasOwnProperty(prop))
            o[prop] = Set._v2s.next++;
        return o[prop];
    }
};
Set._v2s.next = 100;

var set = new Set(4,5,6,'sss', true, {a: 5, b: 6, c: 'ddd'}, {a: 5, b: 6, c: 'ddd'});
console.log(set);