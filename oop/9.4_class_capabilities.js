function Test(){

}

Test.prototype = {
    constructor: 'Test',
    innerFunction: function(){
        console.log('Inner');
    }
};


var test = new Test();
test.innerFunction();

Test.prototype.outer = function(){
    console.log('Outer');
}

test.outer();
console.log(test.constructor);
//Расширение стандартных классов

var n = 3;



Number.prototype.times = function(f, context){
    var n = Number(this);
    for (var i = 1; i <= n; i++){
        f.call(context, i);
    }
};

n.times(function(n){console.log(n)});