function enumeration(namesToValues){
    var enumeration = function(){
        throw  "Нельзя создать экземпляр класса Enumeration";
    };

    var proto = enumeration.prototype = {
        constructor: enumeration,
        toString: function(){return this.name;},
        valueOf: function(){return this.value;},
        toJSON: function(){return this.name;}
    };
    enumeration.values = [];

    for(name in namesToValues) {
       var e = new Object(proto);
       e.name = name;
       e.value = namesToValues[name];
       enumeration[name] = e;
       enumeration.values.push(e);
    }

    enumeration.foreach = function(f, c) {
        for(var i = 0; i <= this.values.length; i++) {
            f.call(c, this.values[i]);
        }
    };

    return enumeration;
}

function Card(suit, rank) {
    this.suit = suit;
    this.rank = rank;
}

Card.Suit = enumeration({Clubs: 1, Diamonds: 2, Hearts: 3, Spades: 4});
Card.Rank = enumeration({Two: 1, Three: 3, Four: 4, Five: 5, Six: 6, Seven: 7, Eight: 8, Night: 9, Ten: 10,
                        Jack: 11, Queen: 12, King: 13, Ace: 14});

Card.prototype.toString = function() {
    return this.rank.toString() + " " + this.suit.toString();
}

Card.prototype.compareTo = function(that) {
    if (this.rank < that.rank) return -1;
    if (this.rank > that.rank) return 1;
    return 0;
}

Card.orderByRank = function(a, b){return a.compareTo(b);};

Cart.orderBySuit = function(a, b) {
    if (a.suit < b.suit) return -1;
    if (a.suit > b.suit) return 1;
    if (a.rank < b.rank) return -1;
    if (a.rank > b.rank) return 1;
    return 0;
}